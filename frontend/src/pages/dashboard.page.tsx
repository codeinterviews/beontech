import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";

const backendClient = new BackendClient();
const INCREMENT = 5;

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [searchValue, setSearchValue] = useState<string>('');
  const [pagination, setPagination] = useState<number>(0);

  const handleOnKeyDown: React.KeyboardEventHandler<HTMLInputElement> = async (e) => {
    if (e.keyCode === 13) {
      setIsLoading(true);

      const result = await backendClient.getAllUsers(searchValue, pagination, INCREMENT);
      setUsers(result.data);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers('', pagination, INCREMENT);
      setUsers(result.data);
      setIsLoading(false);
    };

    fetchData();
  }, []);

  return (
    <div style={{ paddingTop: "30px" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {isLoading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            <input type="text" value={searchValue} 
            onChange={(e) => setSearchValue(e.currentTarget.value)} onKeyDown={handleOnKeyDown} />
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
