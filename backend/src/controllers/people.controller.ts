import {
    JsonController,
    Get,
    HttpCode,
    NotFoundError,
    Param,
    QueryParam
} from 'routing-controllers';
import { PeopleProcessing } from '../services/people_processing.service';

const peopleProcessing = new PeopleProcessing();

@JsonController('/people', { transformResponse: false })
export default class PeopleController {
    @HttpCode(200)
    @Get('/all')
    getAllPeople(
        @QueryParam('filter') filter: string,
        @QueryParam('pagination') pagination: number, 
        @QueryParam('increment') increment: number
    ) {
        const people = peopleProcessing.getAll().filter(p => {
            return p.first_name.includes(filter) || p.email.includes(filter);
        }).slice(pagination * increment, (pagination * increment) + increment);

        if (!people) {
            throw new NotFoundError('No people found');
        }

        return {
            data: people,
        };
    }

    @HttpCode(200)
    @Get('/:id')
    getPerson(@Param('id') id: number) {
        const person = peopleProcessing.getById(id);

        if (!person) {
            throw new NotFoundError('No person found');
        }

        return {
            data: person,
        };
    }
}
